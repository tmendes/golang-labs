package main

import "fmt"

func main() {
    colors := map[string]string{
        "red": "#ff0000",
        "green": "00ff00",
        "blue": "0000ff",
    }

    colors2 := make(map[string]string)

    fmt.Println(colors)

    fmt.Println(colors2)

    colors2["white"] = "#ffffff"
    colors2["red"] = "#ff0000"
    colors2["green"] = "#00ff00"
    colors2["blue"] = "#0000ff"

    fmt.Println(colors2)

    printMap(colors)
    printMap(colors2)

    delete(colors2, "white")

    fmt.Println(colors2)
}

func printMap(m map[string]string) {
    for key, value := range m {
        fmt.Println(key, ":", value)
    }
}
