package main

import "fmt"

type contactInfo struct {
    email string
    zipCode int
}

type person struct {
    firstName string
    lastName string
    contact contactInfo
}

func main() {
    alex := person{
        firstName: "Alex",
        lastName: "Anderson",
        contact: contactInfo{
            email: "alex@anderson.com",
            zipCode: 3020,
        },
    }
    alex.print()

    var rafa person
    rafa.firstName = "Rafa"
    rafa.lastName = "Silva"
    rafa.contact.email = "rafa@silva.com"
    rafa.contact.zipCode = 3040
    rafa.print()

    rafa.updateName("Rafa Fara")
    rafa.print()
}

func (p * person) updateName(newName string) {
    (*p).firstName = newName
}

func (p person) print() {
    fmt.Println(p)
}
