package main

/* package main means we are going to build a binary
 * package */

import "fmt"

/* fmt is a short for format
 * it is usally used to print out */

func main() {
	fmt.Println("Hello world")
}
