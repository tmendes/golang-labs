package main

import "fmt"

type triangle struct {
	height float64
	base   float64
}

type square struct {
	sideLength float64
}

type shape interface {
	getArea() float64
}

func main() {
	s := square{sideLength: 3}
	t := triangle{base: 3, height: 4}
	fmt.Println("Triangle Area: ", getArea(t))
	fmt.Println("Square   Area: ", getArea(s))
}

func getArea(s shape) float64 {
	return s.getArea()
}

func (t triangle) getArea() float64 {
	return 0.5 * t.base * t.height
}

func (s square) getArea() float64 {
	return s.sideLength * s.sideLength
}
