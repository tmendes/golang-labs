package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"http://facebook.com",
		"http://stackoverflow.com",
		"http://amazon.com",
		"http://localhost",
	}

	c := make(chan string)

	for _, link := range links {
		go checkRequest(link, c)
	}

	fmt.Println("--------------------------")

	/* Waiting for channels */
	for i := 0; i < len(links); i++ {
		fmt.Println(<-c)
	}

	/* Different approach */
	for _, link := range links {
		go checkRequest(link, c)
	}

	fmt.Println("--------------------------")

	for l := range c {
		/* function literals */
		go func(link string) {
			time.Sleep(5 * time.Second)
			checkRequest(link, c)
		}(l)
	}

}

func checkRequest(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, "down")
		c <- link
		return
	}

	fmt.Println(link, "up")
	c <- link
}
