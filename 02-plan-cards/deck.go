package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"strings"
	"time"
)

/* Create a new type of deck
 * which is a slice of strings */
type deck []string

func newDeck() deck {
    cards := deck{}
    cardsSuits := []string{"Spades", "Hearts", "Diamonds"}
    cardsValues := []string{"Ace", "Two", "Three", "Four"}
    for _, suit := range cardsSuits {
        for _, value := range cardsValues {
            cards = append(cards, value + " of " + suit)
        }
    }
    return cards
}

/* Create a receiver
 * Set ups methods on variables we create
 * the deck after func means we want to attach the func to this type.
 * the d is a reference to the instances we are working on
 * by convention we call the reference as the first or two first letters
 * of the type */
func (d deck) print() {
    for _, card := range d {
        fmt.Println(card)
    }
}

func deal(d deck, handSize int) (deck, deck) {
    leftHand := d[:handSize]
    rightHand := d[handSize:]
    return leftHand, rightHand
}

func (d deck) toString() string {
    return strings.Join([]string(d), ",")
}

func (d deck) saveToFile(filename string) error {
    message := []byte(d.toString())
    err := ioutil.WriteFile("decks.txt", message, 0644)
    return err
}

func newDeckFromFile(filename string) deck {
    content, err := ioutil.ReadFile(filename)
    if err != nil {
        return newDeck()
    }

    s := strings.Split(string(content), ",")

    return deck(s)
}

func (d deck) shuffle() {
    source := rand.NewSource(time.Now().UnixNano())
    r := rand.New(source)

    for i := range d {
        newPosition := r.Intn(len(d) - 1)
        d[i], d[newPosition] = d[newPosition], d[i]
    }
}
