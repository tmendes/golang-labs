package main

import "fmt"

/* Slice is a reference type because
   go will make a copy of the header
   which contains a reference to an area
   in memory that holds the payload
*/
func main() {
    mySlice := []int{0, 1, 2}
    fmt.Printf("%v\n", len(mySlice))
    changeMySlice(mySlice)
    fmt.Printf("%v\n", len(mySlice))
}

func changeMySlice(s []int) {
    s = append(s, 3)
    s = append(s, 4)
    fmt.Printf("- %v\n", len(s))
}
