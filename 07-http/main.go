package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWritter struct{}

func main() {
	resp, err := http.Get("https://duckduckgo.com/?q=golang")
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(1)
	}
	fmt.Println("Proto :", resp.Proto)
	fmt.Println("status:", resp.Status)

	lw := logWritter{}

	io.Copy(lw, resp.Body)
}

/* Implement the Writer interface */
func (logWritter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("Just wrote this many bytes:", len(bs))
	return len(bs), nil
}
