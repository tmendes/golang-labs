package main

import "fmt"

type bot interface {
    getGreetings() string
    getVersion() float64
    printIntroduceMyself(string) string
}

type englishBot struct {}
type spanishBot struct {}

func main() {
    eb := englishBot{}
    sb := spanishBot{}
    getVersion(eb)
    getVersion(sb)
    printGreeting(eb)
    printGreeting(sb)
    printIntroduceMyself(eb, "Thiago")
    printIntroduceMyself(sb, "Thiago")
}

func printGreeting(b bot) {
    fmt.Println(b.getGreetings())
}

func getVersion(b bot) {
    fmt.Println(b.getVersion())
}

func printIntroduceMyself(b bot, name string) {
    fmt.Println(b.printIntroduceMyself(name))
}

func (eb englishBot) getGreetings() string {
    return "Hello"
}

func (eb englishBot) getVersion() float64 {
    return 0.1
}

func (eb englishBot) printIntroduceMyself(name string) string {
    return "My name is " + name
}

func (sb spanishBot) getGreetings() string {
    return "Hola"
}

func (sb spanishBot) getVersion() float64 {
    return 0.2
}

func (sb spanishBot) printIntroduceMyself(name string) string {
    return "Me llamo " + name
}
